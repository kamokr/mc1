import json
import logging
import time
import uuid
from collections import namedtuple

import cherrypy
import serial

import mc1


PORT = '/dev/ttyACM'
BAUD = 9600
TIMEOUT = 1
REMOTES_CFG = '/mnt/config/mc1-rv434-remotes.json'
API_DTM_EVENT = '/api/dtm/event?type=mc1-rv434&serial={}&outputs={}'
API_DTM_ADD_REMOTE = '/api/dtm/add_remote?type=mc1-rv434&serial={}'


def cs(data):
    result = 0
    for byte in data:
        result ^= byte
    return data+result.to_bytes(1, byteorder='little')


class StatusApi(mc1.Api):
    def GET(self, **kwargs):
        status = [
            self.module.status.connection,
            self.module.status.uuid
        ]
        return '\r\n'.join(status)


Remote = namedtuple('Remote', ['serial', 'active', 'trigger'])


class UpdateApi(mc1.Api):
    @cherrypy.tools.json_in()
    def POST(self, **kwargs):
        logging.debug('rv434: received HTTP {}'.format(cherrypy.request.request_line))
        try:
            uuid = kwargs['uuid']
        except Exception as ex:
            logging.exception(ex)
            return 'missing or invalid argument: uuid'

        try:
            data = cherrypy.request.json['object']
        except Exception as ex:
            logging.exception(ex)
            return 'missing or invalid JSON content'

        self.module.update_remotes(data)

        with open(REMOTES_CFG, 'w') as f:
            data = json.dumps(data, sort_keys=True, indent=4)
            f.write(data)

        self.module.status.uuid = uuid
        self.module.cfg['rv434']['uuid'] = uuid
        self.module.save_config()
        return 'ok'


class TriggerApi(mc1.Api):
    def GET(self, **kwargs):
        logging.debug('rv434: received HTTP {}'.format(cherrypy.request.request_line))
        try:
            out = int(kwargs['out'])
        except:
            return 'missing or invalid argument: out'

        if out < 1 and out > 15:
            return 'invalid argument value: out'

        if self.module.status.connection == 'connected':
            self.module.trigger(out)
            return 'ok'
        else:
            return 'not connected'


class TestApi(mc1.Api):
    def GET(self, **kwargs):
        try:
            serial = int(kwargs['s'])
        except:
            serial = 12345
        try:
            buttons = int(kwargs['b'])
        except:
            buttons = 1
        self.module.check_and_trigger(serial, buttons)


class Object():
    pass


class Module(mc1.Module):
    CFG_SECTION = 'rv434'
    CFG_DEFAULTS = {
        'serial_port': '/dev/ttyACM',
        'uuid': str(uuid.uuid4()),
        'new_remotes_repeat': 3
    }

    def __init__(self, app):
        super().__init__()
        self.port = None
        self.data = b''
        self.command = None
        self.terminate = False
        self.acm = 0

        self.load_config()

        self.new_remotes_repeat = self.cfg['rv434'].getint('new_remotes_repeat')
        self.new_remotes = [0] * self.new_remotes_repeat
        self.serial_port = self.cfg['rv434'].get('serial_port')
        self.status = Object()
        self.status.connection = 'disconnected'
        self.status.uuid = self.cfg['rv434'].get('uuid')
        self.remotes = {} # {serial: trigger} e.g. {'1234': '1248'}

        # define API
        self.api('status', StatusApi)
        self.api('trigger', TriggerApi)
        self.api('update', UpdateApi)
        self.api('test', TestApi)

        # load remote controllers
        try:
            with open(REMOTES_CFG, 'r') as f:
                data = f.read()
                self.update_remotes(json.loads(data))
        except Exception as ex:
            logging.error(f'rv434: {ex}')

    def stop(self):
        self.terminate = True

    def send(self, data):
        self.port.write(cs(data))

    def update_remotes(self, data):
        self.remotes = {}
        for remote in data.values():
            active = bool(remote['active'])
            serial = int(remote['serial'])
            trigger = ''
            for btn in range(1, 5):
                btnout = 0
                for out in range(1, 5):
                    if remote['button{}output{}'.format(btn, out)]:
                        btnout += 2 ** (out-1)
                trigger += '{:x}'.format(btnout)

            self.remotes[serial] = Remote(serial, active, trigger)
        logging.info('rv434: {} active remote controllers'.format(len(self.remotes)))
        # logging.info('{}'.format(self.remotes))

    def run(self):
        logging.debug('rv434: thread started')
        while not self.terminate:
            try:
                port = self.serial_port+str(self.acm)
                logging.info('rv434: opening port {}, baud rate {}'.format(port, BAUD))
                self.port = serial.Serial(port, BAUD, timeout=TIMEOUT)
                time.sleep(1)
                self.port.flushInput()
            except Exception as ex:
                logging.error(f'rv434: {ex}')
                self.acm += 1
                self.acm %= 4
                time.sleep(1)
                continue

            logging.info('rv434: port {} opened, baud rate {}'.format(port, BAUD))
            try:
                while not self.terminate:
                    self.read_data()
            except Exception as ex:
                logging.error(f'rv434: {ex}')

            try:
                self.send(b'MXK')
                self.port.flushOutput()
            except:
                pass
            self.port.close()
            self.status.connection = 'disconnected'

        logging.debug('rv434: thread stopped')

    def read_data(self):
        c = self.port.read(1)
        if c == b'':
            return
        self.data += c

        if self.command == None:
            if len(self.data) >= 4:
                self.data = self.data[1:]
            if self.data == b'MXA':
                self.command = 'mxa'
                self.to_read = 4
            if self.data == b'MXT':
                self.command = 'mxt'
                self.to_read = 13
            if self.data == b'MXN':
                self.command = 'mxn'
                self.to_read = 11
        else:
            if len(self.data) >= self.to_read:
                self.parse_data()
                self.command = None
                self.data = b''
                
    def parse_data(self):
        if self.command == 'mxa':
            self.status.connection = 'connected'
            self.send(b'MXA')

        if self.command == 'mxt' or self.command == 'mxn':
            serial = int.from_bytes(self.data[3:6], byteorder='little', signed=False)
            buttons = int.from_bytes(self.data[9:10], byteorder='little', signed=False)

            if self.command == 'mxt':
                known = 1
            else:
                known = 0

            logging.debug('rv434: received DTM serial={}, buttons={:04b}, known={}'.format(serial, buttons, known))

            self.check_and_trigger(serial, buttons)
            
    def check_and_trigger(self, serial, buttons):
        if serial not in list(self.remotes.keys()):
            logging.info('rv434: remote {} is not known'.format(serial))

            self.new_remotes.append(serial)
            self.new_remotes = self.new_remotes[1:]

            if len(set(self.new_remotes)) <= 1 and self.new_remotes[0] != 0:
                self.notify_add_remote(serial)
                self.new_remotes = [0] * self.new_remotes_repeat
            return
            
        if not self.remotes[serial].active:
            logging.info('rv434: remote {} is not active'.format(serial))
            return

        trigger = self.remotes[serial].trigger

        outputs = 0
        for btn in range(0, 4):
            BUTTON = 2 ** btn
            if buttons & BUTTON == BUTTON:
                outputs |= int(trigger[btn], base=16)

        if outputs == 0:
            logging.info('rv434: remote {} active, but not triggering any outputs for buttons {:04b}'.format(serial, buttons))
        else:
            self.trigger(outputs)
            self.notify_event(serial, outputs)

    def notify_event(self, serial, outputs):
        uri = API_DTM_EVENT.format(serial, outputs)
        cherrypy.engine.publish('mc1-notify-event', uri)

    def notify_add_remote(self, serial):
        uri = API_DTM_ADD_REMOTE.format(serial)
        cherrypy.engine.publish('mc1-notify-event', uri)

    def trigger(self, outputs):
        logging.debug('rv434: sending DTM trigger outputs={:04b}'.format(outputs))
        for o in range(0, 4):
            OUTPUT = 2 ** o
            if outputs & OUTPUT == OUTPUT:
                self.send(b'MXP'+bytes([OUTPUT]))
        logging.debug('rv434: sending DTM trigger outputs={:04b} done'.format(outputs))

