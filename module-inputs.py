import logging
import queue
import time

import requests
import RPi.GPIO as GPIO

import mc1


INPUT_P0 = 11
INPUT_P1 = 12
INPUT_P2 = 13
INPUT_P3 = 15
INPUT_P4 = 16
INPUT_P5 = 18
CHANNEL_MAP = {
    0: INPUT_P0,
    1: INPUT_P1,
    2: INPUT_P2,
    3: INPUT_P3,
    4: INPUT_P4,
    5: INPUT_P5
}
API_DEVICES_EVENT = '/api/devices/event?type=mc1-inputs&channel={}'


class Module(mc1.Module):
    CFG_SECTION = 'inputs'
    CFG_DEFAULTS = {
        'bouncetime': '200'
    }

    def __init__(self, app):
        super().__init__()
        self.terminate = False
        self.q = queue.Queue()

        self.load_config()

        GPIO.setmode(GPIO.BOARD)

        for c, i in CHANNEL_MAP.items():
            GPIO.setup(i, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.add_event_detect(i, GPIO.FALLING,
                callback=lambda i, c=c: self.input_callback(c), bouncetime=100)

    def stop(self):
        self.terminate = True
        GPIO.cleanup()

    def clear_queue(self):
        try:
            while True:
                self.q.get_nowait()
        except:
            pass

    def run(self):
        logging.debug('inputs: thread started')
        last = 0
        bouncetime = int(self.cfg['inputs'].get('bouncetime'))/1000

        while not self.terminate:
            try:
                channel = self.q.get(timeout=1)
            except queue.Empty:
                continue

            # debounce
            input_num = CHANNEL_MAP[channel]
            state = []
            stable = [0,0,0]
            for _ in range(10):
                state.append(GPIO.input(input_num))
                if state[-3:] == stable:
                    break
                time.sleep(0.01)

            if state[-3:] != stable:
                continue

            now = time.time()
            if now - last < bouncetime:
                continue
            last = now

            logging.debug('inputs: channel {} triggered'.format(channel))
            mc1.publish('mc1-notify-event', API_DEVICES_EVENT.format(channel))
            self.clear_queue()
        logging.debug('inputs: thread stopped')

    def input_callback(self, channel):
        self.q.put(channel)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    m = Module(None)
    m.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass

    m.stop()
