import logging
import os
import queue
import threading
import time

import cherrypy
import serial

import mc1


BAUD = 9600
TIMEOUT = 1

NUL = b'\x00'
EOT = b'\x04'
DLE = b'\x10'
DC2 = b'\x12'
DC3 = b'\x13'
ESC = b'\x1b'
GS  = b'\x1d'


class Status():
    DISCONNECTED = 'disconnected'
    ERROR = 'error'
    UNKNOWN = 'unknown'
    READY = 'ready'
    OK = 'ok'
    LOW = 'low'
    EMPTY = 'empty'
    BUSY = 'busy'
    JAM = 'jam'
    VOLTAGE = 'voltage'
    TEMPERATURE = 'temperature'
    CUTTER = 'cutter'
    HEAD_LEVER_OPEN = 'head_lever_open'


class Printer(threading.Thread):
    __dots__ = 640

    def __init__(self, port):
        super().__init__()
        self.images = {}
        self.port = port
        self.serial = None
        self.queue = queue.Queue()

        self.status_device = Status.DISCONNECTED
        self.status_code = ''
        self.status_paper = Status.UNKNOWN

        self.terminate = False

        self.keywords = [
            'image',
            'code128',
            'qrcode',
            'cut',
            'barcode_size',
            'bold',
            'center',
            'feed',
            'inverse_toggle',
            'left',
            'magnify',
            'normal',
            'right'
        ]

    def stop(self):
        self.terminate = True

    def run(self):
        logging.debug('printers: thread {} started'.format(self))
        while not self.terminate:
            try:
                logging.info('printers: opening port {}, baud rate {}'.format(self.port, BAUD))
                self.serial = serial.Serial(self.port, BAUD, timeout=TIMEOUT)
                time.sleep(1)
                self.serial.flushInput()
            except Exception as ex:
                self.status_device = Status.ERROR
                logging.error(ex)
                time.sleep(5)
                continue

            logging.info('printers: port {} opened'.format(self.port))
            self.status_device = Status.READY

            try:
                self.main_loop()
            except Exception as ex:
                logging.error(ex)

            self.serial.close()
            self.status_device = Status.DISCONNECTED
            self.status_code = ''
            self.status_paper = Status.UNKNOWN
            time.sleep(5)

        logging.debug('printers: thread {} stopped'.format(self))

    def main_loop(self):
        while not self.terminate:
            try:
                source = self.queue.get(timeout=2)
            except queue.Empty:
                self.update_status(timeout=2)
            else:
                if self.status_device == Status.ERROR:
                    logging.error('printers: print command ignored, printer status is ERROR')
                elif self.status_device == Status.BUSY:
                    logging.error('printers: print command ignored, printer status is BUSY')
                else:
                    self.exec(source)

    def exec(self, source):
        split = [e.strip() for e in source.split(';') if e.strip() != '']

        self.init()
        self.encoding()
        for expr in split:
            if '=' in expr:
                lvalue, rvalue = expr.split('=')
            else:
                lvalue = expr
                rvalue = None

            if lvalue in self.keywords:
                func = getattr(self, lvalue)
                if rvalue == None:
                    logging.debug('printers: calling {} without arguments'.format(func.__name__))
                    func()
                else:
                    if ',' in rvalue:
                        args = [int(v) for v in rvalue.split(',')]
                        logging.debug('printers: calling {} with arguments {}'.format(func.__name__, args))
                        func(*args)
                    else:
                        # int conversion
                        if lvalue in ['code128', 'qrcode']:
                            args = rvalue
                        else:
                            args = int(rvalue)

                        logging.debug('printers: calling {} with arguments {}'.format(func.__name__, args))
                        func(args)
            else:
                logging.debug('printers: printing {}'.format(lvalue))
                self.text(lvalue)

    def print(self, data):
        self.queue.put(data)

    def init(self):
        pass

    def encoding(self, value=0):
        pass

    def bold(self):
        pass

    def normal(self):
        pass

    def magnify(self, v=1, h=1):
        pass

    def barcode_size(self, width=1, height=80):
        pass

    def code128(self, code):
        pass
    
    def qrcode(self, code):
        pass

    def image(self, image_id=1, width=320):
        pass
            
    def inverse_toggle(self):
        pass

    def left(self):
        pass

    def center(self):
        pass

    def right(self):
        pass

    def text(self, value, end='\n'):
        pass

    def cut(self, partial=1):
        pass

    def feed(self, lines=1):
        pass

    def update_status(self, timeout=1):
        pass


class SaneiPrinter(Printer):
    __dots__ = 576

    # 9. Response Command
    GSa   = GS+b'a'  # Valid / Invalid of automatic status transmission 
    GSr   = GS+b'r'  # Transmit status 
    GSDLE = GS+DLE   # Valid / Invalid of real-time status transmission 
    GSEOT = GS+EOT   # Transmit status in real-time 
    GSE   = GS+b'E'  # Answer the string 
    GSR1  = GS+b'R1' # Check printer status 
    GSR3  = GS+b'R3' # Print start / cut end automatic status transmission 
    GSI   = GS+b'I'  # Send printer ID 
    ESCs  = ESC+b's' # Send a printer information 
    ESCv  = ESC+b'v' # Send a printer status in the present 

    # GS R 1
    # response DLE STX byte1 byte2 byte3 byte4 DLE ETX
    # byte 1:
    # 'R' - ready
    # 'B' - busy
    # 'E' - error
    # byte 2:
    # '0' - normal
    # '1' - near-end
    # '2' - paper out
    # '3' - head lever open
    # '4' - see byte 3
    # '5' - auto cutter error
    # '6' - reserved
    # '7' - waiting for removal of paper *1
    # '8' - paper on paper detector
    # byte 3:
    # '0' - not applicable
    # '1' - voltage abnormal
    # '2' - temperature abnormal
    # '3' - paper jam
    # byte 4:
    # '0' - reserved

    def __init__(self, port):
        super().__init__(port)

        self.__inverse = False

        # self.images[0] = [
        #     b'\x01\x03\x07\x0f\x1f\x3f\x7f\xff\x7f\x3f\x1f\x0f\x07\x03\x01',
        #     b'\x00\x00\x00\x00\xf0\xf0\xf0\xf0\xf0\xf0\xf0\x00\x00\x00\x00'
        # ]

        self.images[1] = [
            b'\x01\x03\x07\x0f\x1f\x3f\x7f\xff\x7f\x3f\x1f\x0f\x07\x03\x01',
            b'\x00\x00\x00\x00\xf0\xf0\xf0\xf0\xf0\xf0\xf0\x00\x00\x00\x00'
        ]

    def init(self):
        self.__inverse = False
        self.serial.write(ESC+b'@')

    def encoding(self, value=3):
        self.serial.write(ESC+b't'+bytes([value]))

    def bold(self):
        self.serial.write(ESC+b'E\x01')

    def normal(self):
        self.serial.write(ESC+b'E\x00')

    def magnify(self, v=1, h=1):
        v -= 1
        h -= 1
        assert v>=0 and v<=2 and h>=0 and h<=2
        value = bytes([h*16+v])
        self.serial.write(GS+b'!'+value)

    def barcode_size(self, width=1, height=80):
        assert width >= 1 and width <= 4
        assert height >= 1 and height <= 255

        self.serial.write(GS+b'h'+bytes([height]))
        self.serial.write(GS+b'w'+bytes([width])) 

    def code128(self, code, width=1, height=1):
        code = str(code).encode('cp852')
        self.serial.write(GS+b'k\x07g'+code+b'\x00')

    def qrcode(self, code):
        # [コード] <1D> <51> n 各種パラメータ
        # n=0: 設定禁止
        # n=1: 設定禁止
        # n=2: PDF417
        # n=3: MicroPDF417
        # n=4: DataMatrix
        # n=5: MaxiCode
        # n=6: QRCode

        # [コード] <1D> <51> 6 Size ECC̲LV nl nh Data

        # increase cell size
        self.serial.write(GS+b'S\x01')

        size = 5 # 1- 14
        ecc_lv = 3 # 1 - 4
        nl = len(code) % 256
        nh = len(code) // 256
        code = str(code).encode('cp852')

        data = GS+b'Q'
        data += b'\x06' # QR
        data += bytes([size])
        data += bytes([ecc_lv])
        data += bytes([nl])
        data += bytes([nh])
        data += code

        self.serial.write(data)

    def image(self, image_id=1, width=288):
        self.left()
        try:
            image_lines = self.images[image_id]
        except:
            self.text('image={} not found'.format(image_id))
            return

        if self.__inverse:
            image_lines = reversed(image_lines)

        for image_line in image_lines:
            # offset to move the image to the center
            buf = b'\x00' * ((self.__dots__ - width) // 2)
            logging.debug(image_line)
            for c in image_line:
                # stretch image data to fill width
                buf += bytes([c]) * (width // len(image_line))
            # n is length of buf, high and low byte
            nl = len(buf) % 256
            nh = len(buf) // 256

            buf = ESC + b'*\x01' + bytes([nl]) + bytes([nh]) + buf + b'\n'

            self.serial.write(buf)

    def inverse_toggle(self):
        self.serial.write(ESC+b'{\x01')
        self.__inverse = not self.__inverse

    def left(self):
        self.serial.write(ESC+b'a\x00')

    def center(self):
        self.serial.write(ESC+b'a\x01')

    def right(self):
        self.serial.write(ESC+b'a\x02')

    def text(self, value, end='\n'):
        end = end.encode('cp852')
        value = value.encode('cp852')
        self.serial.write(value+end)

    def cut(self, partial=1):
        # self.feed(3)
        if partial == 1:
            self.serial.write(ESC+b'm')
        else:
            self.serial.write(ESC+b'i')

    def feed(self, lines=1):
        assert lines >= 0 and lines <= 255
        self.serial.write(ESC+b'd'+bytes([lines]))

    def update_status(self, timeout=1):
        self.serial.flushInput()
        self.serial.write(self.GSR1)

        t0 = time.time()

        data = b''
        while time.time() - t0 < timeout:
            c = self.serial.read(1)
            if c == b'':
                continue
            data += c

            if len(data) >= 8:
                code = data[2:6].decode('ascii')
                self.status_code = code
                self.status_device = {
                    'R': Status.READY,
                    'B': Status.BUSY,
                    'E': Status.ERROR
                }[code[0]]
                try:
                    self.status_paper = {
                        '0': Status.OK,
                        '1': Status.LOW,
                        '2': Status.EMPTY,
                        '3': Status.HEAD_LEVER_OPEN
                    }[code[1]]
                except:
                    self.status_paper = Status.UNKNOWN
                return
        self.status_code = Status.DISCONNECTED


class ModuleApi(mc1.Api):
    @cherrypy.tools.json_out()
    def GET(self, *args, **kwargs):
        result = {}
        for port, printer in self.module.printers.items():
            result[port] = {'device': printer.status_device, 'paper': printer.status_paper, 'code': printer.status_code}
        return result


class PrintApi(mc1.Api):
    @cherrypy.tools.json_in()
    def POST(self, *args, **kwargs):
        try:
            to_print = cherrypy.request.json
        except Exception as ex:
            logging.exception(ex)
            return 'missing or invalid JSON content'

        logging.info('printers: received {}'.format(to_print))
        
        result = 'ok'
        for port in to_print.keys():
            try:
                printer = self.module.printers[port]
            except KeyError:
                logging.warning('printers: invalid port for print command: {}'.format(port))
                result = 'one or more ports are invalid'
            else:
                printer.print(to_print[port])

        return result


class Module(mc1.Module):
    def __init__(self, app):
        super().__init__()
        self.terminate = False
        self.printers = {}

        self.api(ModuleApi)
        self.api('print', PrintApi)

    def stop(self):
        self.terminate = True

        for printer in self.printers.values():
            printer.stop()

    def run(self):
        logging.debug('printers: thread started')
        while not self.terminate:
            # find /dev/ttySANEI*, e.g /dev/ttySANEI1.1.2
            try:
                ports = [os.path.join('/dev/r300', f) for f in os.listdir('/dev/r300') if f.startswith('printer_sanei')]
            except Exception as ex:
                ports = []

            # add new printers
            for port in set(ports) - set(self.printers):
                logging.warning('printers: new printer {} found'.format(port))
                self.printers[port] = SaneiPrinter(port)
                self.printers[port].start()

            # remove disconnected printers
            for port in set(self.printers) - set(ports):
                logging.warning('printers: printer {} disconnected'.format(port))
                printer = self.printers.pop(port, None)
                if printer:
                    printer.stop()

            # if ports == []:
            #     logging.warning('printer: no printer found')

            time.sleep(5)
        logging.debug('printers: thread stopped')




