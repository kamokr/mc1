import logging
import os
import threading
import time

import cherrypy
import serial

import mc1


BAUD = 9600
TIMEOUT = 1


API_DEVICES_EVENT = '/api/devices/event?type=mc1-scanner&code={}&path={}'


class ScannerApi(mc1.Api):
    @cherrypy.tools.json_out()
    def GET(self, *args, **kwargs):
        result = {}
        for port, scanner in self.module.scanners.items():
            result[port] = scanner.status
        return result


class Module(mc1.Module):
    def __init__(self, app):
        super().__init__()
        self.app = app
        self.terminate = False
        self.scanners = {}

        self.api(ScannerApi)

    def stop(self):
        self.terminate = True

        for scanner in self.scanners.values():
            scanner.stop()

    def run(self):
        logging.debug('scanners: thread started')
        while not self.terminate:
            try:
                ports = [os.path.join('/dev/r300', f) for f in os.listdir('/dev/r300') if f.startswith('scanner')]
            except:
                ports = []

            # add new scanners
            for port in set(ports) - set(self.scanners):
                logging.warning('scanners: new scanner {} found'.format(port))
                self.scanners[port] = Scanner(port)
                self.scanners[port].start()

            # remove disconnected scanners
            for port in set(self.scanners) - set(ports):
                logging.warning('scanners: scanner {} disconnected'.format(port))
                scanner = self.scanners.pop(port, None)
                if scanner:
                    scanner.stop()

            # if ports == []:
            #     logging.warning('scanner: no scanner found')

            time.sleep(5)
        logging.debug('scanners: thread stopped')


class Status():
    DISCONNECTED = 'disconnected'
    ERROR = 'error'
    READY = 'ready'
    UNKNOWN = 'unknown'


class Scanner(threading.Thread):
    def __init__(self, port):
        super().__init__()
        self.port = port
        self.terminate = False
        self.status = Status.UNKNOWN

    def stop(self):
        self.terminate = True

    def run(self):
        logging.debug('scanners: thread {} started'.format(self))
        while not self.terminate:
            try:
                logging.info('scanners: opening port {}, baud rate {}'.format(self.port, BAUD))
                self.serial = serial.Serial(self.port, BAUD, timeout=TIMEOUT)
                time.sleep(1)
                self.serial.flushInput()
            except Exception as ex:
                self.status = Status.ERROR
                logging.error(ex)
                time.sleep(5)
                continue

            logging.info('scanners: port {} opened'.format(self.port))
            self.status = Status.READY

            try:
                self.main_loop()
            except Exception as ex:
                logging.error(ex)

            self.serial.close()
            self.status = Status.DISCONNECTED
            time.sleep(5)

        logging.debug('scanners: thread {} stopped'.format(self))
  
    def main_loop(self):
        data = b''
        while not self.terminate:
            c = self.serial.read(1)
            if c == b'':
                continue
            if c == b'\r':
                try:
                    code = data.decode('ascii')
                    mc1.publish('mc1-notify-event', API_DEVICES_EVENT.format(code, self.port))
                except Exception as ex:
                    logging.exception(ex)
                data = b''
                continue
            data += c


    