import logging
import queue
import threading
import time

import cherrypy
import RPi.GPIO as GPIO

import mc1


RELAY_CH1 = 37
RELAY_CH2 = 38
RELAY_CH3 = 40
CHANNEL_MAP = {
    1: RELAY_CH1, 
    2: RELAY_CH2,
    3: RELAY_CH3
}


class OutputThread(threading.Thread):
    def __init__(self, channel, duration):
        super().__init__()
        self.channel = channel
        self.duration = duration
        self.terminate = False
        self.q = queue.Queue()

    def __repr__(self):
        return '{}(channel={}, duration={})'.format(self.__class__.__name__, self.channel, self.duration)

    def stop(self):
        logging.debug('outputs: {} stopping'.format(self))
        self.terminate = True

    def run(self):
        logging.debug('outputs: {} started'.format(self))
        while not self.terminate:
            try:
                duration = self.q.get(timeout=1)
            except queue.Empty:
                continue
            self.do_trigger(duration)
            self.clear_queue()
        logging.debug('outputs: {} stopped'.format(self))

    def clear_queue(self):
        try:
            while True:
                self.q.get_nowait()
        except:
            pass
        
    def trigger(self, duration=None):
        if self.terminate:
            return
        self.q.put(duration)

    def open(self):
        logging.debug('outputs: opening channel {}'.format(self.channel))
        GPIO.output(self.channel, GPIO.HIGH)

    def close(self):
        logging.debug('outputs: closing channel {}'.format(self.channel))
        GPIO.output(self.channel, GPIO.LOW)

    def do_trigger(self, duration):
        if self.terminate:
            return

        logging.debug('outputs: triggering channel {}'.format(self.channel))
        GPIO.output(self.channel, GPIO.LOW)
        if duration == None:
            duration = self.duration
        time.sleep(duration)
        GPIO.output(self.channel, GPIO.HIGH)


class Module(mc1.Module):
    CFG_SECTION = 'outputs'
    CFG_DEFAULTS = {
        'duration': '0.5'
    }

    def __init__(self, app):
        super().__init__()
        self.q = queue.Queue()
        self.load_config()
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(RELAY_CH1, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(RELAY_CH2, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.setup(RELAY_CH3, GPIO.OUT, initial=GPIO.HIGH)
        self.api('trigger', TriggerApi)
        self.api('open', OpenApi)
        self.api('close', CloseApi)

        duration = float(self.cfg['outputs'].get('duration'))
        self.threads = {}
        for relay_ch in CHANNEL_MAP.values():
            self.threads[relay_ch] = OutputThread(relay_ch, duration)

    def start(self):
        for relay_ch in CHANNEL_MAP.values():
            self.threads[relay_ch].start()

    def stop(self):
        for relay_ch in CHANNEL_MAP.values():
            self.threads[relay_ch].stop()
        for relay_ch in CHANNEL_MAP.values():
            self.threads[relay_ch].join()
        GPIO.cleanup()

    def trigger(self, relay_ch, duration=None):
        self.threads[relay_ch].trigger(duration)

    def open(self, relay_ch):
        self.threads[relay_ch].open()

    def close(self, relay_ch):
        self.threads[relay_ch].close()


class TriggerApi(mc1.Api):
    def GET(self, **kwargs):
        return self.POST(**kwargs)

    def POST(self, **kwargs):
        try:
            relay_ch = CHANNEL_MAP[int(kwargs['id'])]
        except:
            return 'invalid channel'

        try:
            duration = float(kwargs['duration'])
        except:
            duration = None

        self.module.trigger(relay_ch, duration=duration)
        return 'ok'


class OpenApi(mc1.Api):
    def GET(self, **kwargs):
        return self.POST(**kwargs)

    def POST(self, **kwargs):
        try:
            relay_ch = CHANNEL_MAP[int(kwargs['id'])]
        except:
            return 'invalid channel'

        self.module.open(relay_ch)
        return 'ok'


class CloseApi(mc1.Api):
    def GET(self, **kwargs):
        return self.POST(**kwargs)

    def POST(self, **kwargs):
        try:
            relay_ch = CHANNEL_MAP[int(kwargs['id'])]
        except:
            return 'invalid channel'

        self.module.close(relay_ch)
        return 'ok'







