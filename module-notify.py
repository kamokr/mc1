import logging
import queue
import threading

import requests

import mc1


class Module(mc1.Module):
    def __init__(self, app):
        super().__init__()
        self.terminate = False
        self.q = queue.Queue()
        self.server_ipv4 = app.cfg['system'].get('server_ipv4')
        self.server_port = app.cfg['system'].get('server_port')
        mc1.subscribe('mc1-notify-event', self.post)

    def stop(self):
        self.terminate = True

    def post(self, uri):
        logging.debug('notify: queueing HTTP POST {}'.format(uri))
        self.q.put(uri)

    def run(self):
        logging.debug('notify: thread started')
        while not self.terminate:
            try:
                uri = self.q.get(timeout=1)
            except queue.Empty:
                continue
            logging.debug('notify: sending HTTP POST {}'.format(uri))
            try:
                requests.post('http://{}:{}{}'.format(self.server_ipv4, self.server_port, uri), timeout=1)
            except Exception as ex:
                logging.error('notify: sending HTTP POST error: {}'.format(ex))
            logging.debug('notify: sending HTTP POST {} done'.format(uri))
        logging.debug('notify: thread stopped')

            
