import logging
logging.getLogger().setLevel(logging.DEBUG)
import time

import serial

import mc1


PORT = '/dev/ttyACM'
BAUD = 9600
TIMEOUT = 1


NUL = b'\x00'
EOT = b'\x04'
DLE = b'0x10'
DC2 = b'0x12'
DC3 = b'0x13'
ESC = b'\x1b'
GS  = b'\x1d'

class SaneiEscPos():
    # 9. Response Command
    GSa   = GS+b'a'  # Valid / Invalid of automatic status transmission 
    GSr   = GS+b'r'  # Transmit status 
    GSDLE = GS+DLE   # Valid / Invalid of real-time status transmission 
    GSEOT = GS+EOT   # Transmit status in real-time 
    GSE   = GS+b'E'  # Answer the string 
    GSR1  = GS+b'R1' # Check printer status 
    GSR3  = GS+b'R3' # Print start / cut end automatic status transmission 
    GSI   = GS+b'I'  # Send printer ID 
    ESCs  = ESC+b's' # Send a printer information 
    ESCv  = ESC+b'v' # Send a printer status in the present 

    # GS R 1
    # response DLE STX byte1 byte2 byte3 byte4 DLE ETX
    # byte 1:
    # 'R' - ready
    # 'B' - busy
    # 'E' - error
    # byte 2:
    # '0' - normal
    # '1' - near-end
    # '2' - paper out
    # '3' - head lever open
    # '4' - see byte 3
    # '5' - auto cutter error
    # '6' - reserved
    # '7' - waiting for removal of paper *1
    # '8' - paper on paper detector
    # byte 3:
    # '0' - not applicable
    # '1' - voltage abnormal
    # '2' - temperature abnormal
    # '3' - paper jam
    # byte 4:
    # '0' - reserved


class Module(mc1.Module):
    # CFG = '/mc1-printer.cfg'

    def __init__(self, parent):
        super().__init__()

        self.terminate = False
        self.port = None
        self.data = b''
        self.prev_status = None

    def stop(self):
        self.terminate = True

    def run(self):
        logging.debug('printer: thread started')
        port_n = 0

        while not self.terminate:
            try:
                port = '/dev/ttyACM{}'.format(port_n)
                logging.info('printer: opening port {}, baud rate {}'.format(port, BAUD))
                self.port = serial.Serial(port, BAUD, timeout=TIMEOUT)
                time.sleep(1)
                
            except serial.SerialException as ex:
                logging.error(ex)
                time.sleep(1)
                port_n += 1
                port_n %= 4
                continue


            logging.info('printer: port {} opened, baud rate {}'.format(port, BAUD))
            # self.port.write(b'\x1dR1')



            # self.port.write(SaneiEscPos.GSa)
            # self.port.write(SaneiEscPos.GSR1)

            try:
                while not self.terminate:
                    self.read_status()
            except Exception as ex:
                logging.error(ex)

            self.port.flushOutput()
            self.port.close()

        logging.debug('rv434: thread stopped')

    def print(self):
        self.port.write()

    def read_status(self):
        self.port.flushInput()
        self.port.write(SaneiEscPos.GSR1)

        while not self.terminate:
            c = self.port.read(1)
            if c == b'':
                continue
            self.data += c

            if len(self.data) >= 8:
                # print(self.data)
                # self.data = b''
                # continue

                status = self.data[2:6].decode('ascii')
                if status != self.prev_status:
                    print('status changed:', status)
                    self.prev_status = status
                self.data = b''
                time.sleep(1)
                break

                # status = []
                # if self.data[2] == 82: # b'R'
                #     status.append('PAPER OK')
                # elif self.data[2] == 69: # b'E'
                #     status.append('NO PAPER')
                # if self.data[3] == 49:
                #     status.append('PAPER LOW') 
                # print(', '.join(status))
                # self.data = b''
                # time.sleep(1)
                # break






def sanei_print(name, ticketid, ts, plate, filename):
    if not os.path.exists(filename):
        logging.error('DEVICES printer {} does not exist'.format(filename))
        return
    with open(filename, 'wb') as f:
        ticketid = bytes(str(ticketid).encode('utf-8'))
        plate = bytes(plate.encode('utf-8'))
        datestr = bytes(datetime.datetime.fromtimestamp(ts/1000).strftime('%d.%m.%Y').encode('utf-8'))
        timestr = bytes(datetime.datetime.fromtimestamp(ts/1000).strftime('%H:%M:%S').encode('utf-8'))
        name = bytes(name.encode('utf-8'))

        f.write(ESC+b'@')

        # ticket number        
        f.write(ESC+b'{\x01') # print inverse
        f.write(ESC+b'a\x01') # center
        f.write(b'SEECON C1 - EWIDENCJA WJAZDU/WYJAZDU\n')
        f.write(GS+b'!\x11') # vertical and horizontal magnification
        f.write(ticketid+b'\n')

        # CODE128
        f.write(GS+b'h\xA0')
        f.write(GS+b'w\x02')
        f.write(GS+b'k\x07g'+ticketid+b'\x00')
        f.write(datestr+b'\t'+timestr+b'\n')
        f.write(plate+b'\n')
        f.write(ESC+b'E\x01') # bold
        f.write(GS+b'!\x00') # vertical and horizontal magnification
        f.write(name+b'\n')
        f.write(ESC+b'E\x00') # not bold
        f.write(ESC+b'd\x06') # blank line
        f.write(ESC+b'm') # partial cut

        f.flush()


# a = Module(None)
# try:
#     a.start()
# except KeyboardInterrupt:
#     a.stop()

from contextlib import contextmanager


class SaneiPrinter():
    def __init__(self):
        self.port = None


    @contextmanager
    def open(self, port, baud=9600, timeout=1):
        self.port = serial.Serial(port, baud, timeout=timeout)
        yield self
        self.port.flushOutput()
        self.port.close()

    def init(self):
        self.port.write(ESC+b'@')

    def encoding(self, value=3):
        self.port.write(ESC+b't'+bytes([value]))

    def bold(self):
        self.port.write(ESC+b'E\x01')

    def normal(self):
        self.port.write(ESC+b'E\x00')

    def magnify(self, v=1, h=1):
        v -= 1
        h -= 1
        assert v>=0 and v<=2 and h>=0 and h<=2
        value = bytes([h*16+v])
        self.port.write(GS+b'!'+value)

    def barcode_size(self, width=1, height=80):
        assert width >= 1 and width <= 4
        assert height >= 1 and height <= 255

        self.port.write(GS+b'h'+bytes([height]))
        self.port.write(GS+b'w'+bytes([width])) 

    def code128(self, code, width=1, height=1):
        code = str(code).encode('cp852')
        self.port.write(GS+b'k\x07g'+code+b'\x00')

    def qrcode(self, code):
        # [コード] <1D> <51> n 各種パラメータ
        # n=0: 設定禁止
        # n=1: 設定禁止
        # n=2: PDF417
        # n=3: MicroPDF417
        # n=4: DataMatrix
        # n=5: MaxiCode
        # n=6: QRCode

        # [コード] <1D> <51> 6 Size ECC̲LV nl nh Data

        # increase cell size
        self.port.write(GS+b'S\x01')

        size = 5 # 1- 14
        ecc_lv = 3 # 1 - 4
        nl = len(code) % 256
        nh = len(code) // 256
        code = str(code).encode('cp852')


        data = GS+b'Q'
        data += b'\x06' # QR
        data += bytes([size])
        data += bytes([ecc_lv])
        data += bytes([nl])
        data += bytes([nh])
        data += code

        self.port.write(data)

    def arrow(self, dots=576, width=288):
        arrow = [
            b'\x00',
            b'\x00',
            b'\x00',
            b'\x00',
            b'\xf0',
            b'\xf0',
            b'\xf0',
            b'\xf0',
            b'\xf0',
            b'\xf0',
            b'\xf0',
            b'\x00',
            b'\x00',
            b'\x00',
            b'\x00'
        ]

        buf = b'\x00' * ((dots-width)//2)
        for c in arrow:
            buf += c * (width//len(arrow))

        nl = len(buf) % 256
        nh = len(buf) // 256
        buf = ESC+b'*\x01'+bytes([nl])+bytes([nh])+buf+b'\n'

        self.port.write(buf)

        arrow = [
            b'\x01',
            b'\x03',
            b'\x07',
            b'\x0f',
            b'\x1f',
            b'\x3f',
            b'\x7f',
            b'\xff',
            b'\x7f',
            b'\x3f',
            b'\x1f',
            b'\x0f',
            b'\x07',
            b'\x03',
            b'\x01'
        ]

        buf = b'\x00' * ((dots-width)//2)
        for c in arrow:
            buf += c * (width//len(arrow))

        nl = len(buf) % 256
        nh = len(buf) // 256
        buf = ESC+b'*\x01'+bytes([nl])+bytes([nh])+buf+b'\n'

        self.port.write(buf)

    def inverse_toggle(self):
        self.port.write(ESC+b'{\x01')

    def left(self):
        self.port.write(ESC+b'a\x00')

    def center(self):
        self.port.write(ESC+b'a\x01')

    def right(self):
        self.port.write(ESC+b'a\x02')

    def print(self, value, end='\n'):
        end = end.encode('cp852')
        value = value.encode('cp852')
        self.port.write(value+end)

    def cut(self, partial=False):
        if partial:
            self.port.write(ESC+b'm')
        else:
            self.port.write(ESC+b'i')

    def feed(self, lines=1):
        assert lines >= 0 and lines <= 255
        self.port.write(ESC+b'd'+bytes([lines]))

    def read_status(self, timeout=1):
        self.port.flushInput()
        self.port.write(SaneiEscPos.GSR1)

        t0 = time.time()

        data = b''
        while time.time()-t0 < timeout:
            c = self.port.read(1)
            if c == b'':
                continue
            data += c

            if len(data) >= 8:
                status = data[2:6].decode('ascii')
                return status

        return None

# try:
#     with SaneiPrinter().open('/dev/ttyACM0') as p:
#         p.init()
#         p.inverse_toggle()
#         p.align_center()
#         # p.magnify(1, 2)
#         # p.align_center()
#         # p.print('test')


#         p.print('(C) 2019 LANTEQ')
#         p.print('SEECON MC1')

#         p.print('2019-10-17T12:12:12')

#         p.bold()
#         p.print('PLYWALNIA DELFIN')


#         p.bold(False)

#         p.feed(6)


#         code = '1234567890 1234'
#         p.print(code)
#         p.barcode(code)

#         p.align_left()

#         p.print_arrow()
#         p.feed(3)
#         p.cut(partial=True)    


# except serial.SerialException as ex:
#     print(ex)

import datetime
import queue
import threading

class Printer3(threading.Thread):
    def __init__(self, printer_class):
        self.q = queue.Queue()
        self.printer = printer_class()
        self.terminate = False
        self.status = None

    def stop(self):
        self.terminate = True

    def run(self):
        while not self.terminate:
            try:
                cmd = self.q.get(timeout=2)
            except queue.Empty:
                with self.printer.open('/dev/ttyACM0') as p:
                    status = p.read_status(timeout=2)



TICKET_FORMAT = '''
inverse_toggle
center
{isodatetime}
SEECON MC1 (C) 2019 LANTEQ
feed
tel. 22 631 48 67
01-211 Warszawa
ul. Kasprzaka 1/3 
magnify=1,2
bold
{name}
magnify
normal
feed
TĄ STRONĄ, KODEM DO GÓRY
PRZY WYJEŹDZIE WSUNĄĆ DO CZYTNIKA
left
arrow
center
feed=2
{unixts:011d} {ticket:05d}
code128={unixts:011d} {ticket:05d}
'''

TICKET_FORMAT2 = '''
inverse_toggle
center
SEECON MC1 (C) 2019 LANTEQ
feed=2
tel. 22 631 48 67
01-211 Warszawa
ul. Kasprzaka 1/3 
magnify=1,2
bold
{name}
magnify
normal
feed=2
{isodatetime}
{unixts:011d} {ticket:05d}
code128={unixts:011d} {ticket:05d}
left
arrow
'''


TICKET_FORMAT2 = '''
inverse_toggle
center
SEECON C1 - EWIDENCJA WJAZDU/WYJAZDU
magnify=2,2
{ticket:010d}
barcode_size=2,80
barcode={ticket:010d}
{date}\t{time}
WF5800A
bold
magnify=1,1
PANATTONI
feed=3'''

# ticket description mini-language
class Printer():
    NO_CUT = 0
    PARTIAL_CUT = 1
    FULL_CUT = 2

    def __init__(self, printer):
        self.printer = printer

        self.__mapping__ = {
            'arrow': printer.arrow,
            'code128': printer.code128,
            'barcode_size': printer.barcode_size,
            'bold': printer.bold,
            'center': printer.center,
            'feed': printer.feed,
            'inverse_toggle': printer.inverse_toggle,
            'left': printer.left,
            'magnify': printer.magnify,
            'normal': printer.normal,
            'right': printer.right
        }

    def print(self, tdl, name='', cut=0, ts=None):
        if ts:
            unixts = ts//1000
            dtinfo = datetime.datetime.fromtimestamp(unixts)
        else:
            dtinfo = datetime.datetime.now()

        unixts = int(time.time())
        datestr = dtinfo.strftime('%d.%m.%Y')
        timestr = dtinfo.strftime('%H:%M:%S')
        isodatetime = dtinfo.replace(microsecond=0).isoformat()


        source = tdl.format(
            isodatetime=isodatetime,
            unixts=unixts,
            ticket=1234,
            date=datestr,
            name=name,
            time=timestr)

        split = [e.strip() for e in source.split('\n') if e.strip() != '']

        self.printer.init()
        self.printer.encoding()
        for expr in split:
            if '=' in expr:
                lvalue, rvalue = expr.split('=')
            else:
                lvalue = expr
                rvalue = None

            if lvalue in self.__mapping__.keys():
                func = self.__mapping__[lvalue]
                if rvalue == None:
                    print('calling {} without arguments'.format(func.__name__))
                    func()
                else:
                    if ',' in rvalue:
                        args = [int(v) for v in rvalue.split(',')]
                        print('calling {} with arguments {}'.format(func.__name__, args))
                        func(*args)
                    else:
                        # int conversion
                        if lvalue in ['code128']:
                            args=rvalue
                        else:
                            args = int(rvalue)

                        print('calling {} with arguments {}'.format(func.__name__, args))
                        func(args)
            else:
                print('printing {}'.format(lvalue))
                self.printer.print(lvalue)

        self.printer.feed(3)
        if cut == Printer.PARTIAL_CUT:
            self.printer.cut(partial=True)
        elif cut == Printer.FULL_CUT:
            self.printer.cut(partial=False)



with SaneiPrinter().open('/dev/ttyACM0') as p:
    Printer(p).print(
        TICKET_FORMAT,
        # name='ZAŻÓŁĆ GĘŚLĄ JAŹŃ',
        name='PŁYWALNIA DELFIN',
        cut=Printer.PARTIAL_CUT)

    