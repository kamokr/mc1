import ipaddress
import logging
import os
import subprocess

import netifaces


def set_ip(iface, mode, addr, mask, gw=None, dns1=None, dns2=None):
    YAML_FILE = '/etc/netplan/01-netcfg.yaml'
    #YAML_FILE = '/home/kamil/app/c1.1-dev/util/01-netcfg.yaml'

    logging.info('CORE setting server interface {} IP address {}/{} gateway {} dns {}, {}'.format(iface, addr, mask, gw, dns1, dns2))

    if iface not in netifaces.interfaces():
        logging.error('CORE invalid interface {}'.format(iface))
        raise ValueError('"{}" is not a valid interface'.format(iface))

    if mode not in ['static', 'dhcp']:
        logging.error('CORE invalid mode {}'.format(mode))
        raise ValueError('"{}" is not a valid mode, only "static" or "dhcp" is allowed'.format(mode))
        
    for check in [address for address in [addr, gw, dns1, dns2] if address != None]:
        ipaddress.ip_address(check)

    dnss = ','.join([dns for dns in [dns1, dns2] if dns != None])

    yaml = ''
    yaml += 'network:\n'
    yaml += ' version: 2\n'
    yaml += ' renderer: networkd\n'
    yaml += ' ethernets:\n'
    yaml += '  {iface}:\n'
    yaml += '   dhcp4: no\n'
    yaml += '   dhcp6: no\n'
    yaml += '   addresses: [{addr}/{mask}]\n'
    if gw != None:
        yaml += '   gateway4: {gw}\n'
    if dnss != '':
        yaml += '   nameservers:\n'
        yaml += '    addresses: [{dnss}]\n'

    #print(yaml)
    data = yaml.format(iface=iface, addr=addr, mask=mask, gw=gw, dnss=dnss)
    #print(data)

    with open(YAML_FILE, 'w') as f:
        f.write(data)

    out = subprocess.check_output('sudo -n netplan --debug apply', shell=True)
    out = out.split(b'\n')
    for line in out:
        #print(line.decode('utf-8'))
        logging.debug('CORE {}'.format(line.decode('utf-8')))