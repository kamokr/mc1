import os

import cherrypy

import mc1


def measure_temp():
        temp = os.popen('vcgencmd measure_temp').readline()
        return (temp.replace('temp=', '').replace('\'C', '').strip())


class StatusApi(mc1.Api):
    @cherrypy.tools.json_out()
    def GET(self, **kwargs):
        return {
            'temp': measure_temp(),
            'modules': list(self.module.parent.modules.keys())
        }


class Module(mc1.Module):
    def __init__(self, parent):
        mc1.Module.__init__(self)
        self.parent = parent

        self.api(StatusApi)