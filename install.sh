#!/usr/bin/env bash

APPD="/root/app/"
NAME="mc1"

echo ":: installing dependencies"
apt install git
apt install python3
apt install python3-pip
pip3 install cherrypy
pip3 install python-daemon
pip3 install netifaces
pip3 install requests
pip3 install pyserial
pip3 install RPi.GPIO
cp mc1.service /etc/systemd/system
chmod 664 /etc/systemd/system/mc1.service
cp 98-c1-system.rules /etc/udev/rules.d/

# read-only mode
# /etc/fstab
#tmpfs /tmp tmpfs defaults,size=488M,noatime,nodev,nosuid,mode=1777 0 0
#tmpfs /DietPi tmpfs defaults,size=10m,noatime,nodev,nosuid,mode=1777 0 0
#tmpfs /var/log tmpfs defaults,size=50m,noatime,nodev,nosuid,mode=1777 0 0
#tmpfs /var/tmp tmpfs defaults,size=50m,noatime,nodev,nosuid,mode=1777 0 0
#PARTUUID=27504eef-02 / auto defaults,noatime,ro 0 1
#PARTUUID=27504eef-01 /boot auto defaults,noatime,ro 0 1

# /boot/cmdline.txt
#dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=PARTUUID=27504eef-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait fastboot noswap ro

# remote plymouth which is blocking boot and completely unnecessary
#apt remove plymouth

# link to ram
#ln -s /tmp /var/run
#ln -s /tmp /var/spool
#ln -s /tmp /var/lock

# link to resolv.conf
#ln -s /etc/resolvconf/run/resolv.conf /etc/resolv.conf


# remount as rw
#mount -o remount,rw /

