#!/usr/bin/env python3

# MC1
# version: 0.2
# Copyright (c) 2018-2019 LANTEQ Kamil Okrasa

import argparse
import configparser
import importlib
import logging
import logging.config
import os
import signal
import sys
import threading
import time
import types
from collections import OrderedDict

import cherrypy
cherrypy.config.update({
    'global': {
        'environment' : 'embedded',
    }
})
import daemon
import lockfile.pidlockfile

CFGFILE = '/mnt/config/mc1.ini'
PIDFILE = '/tmp/mc1.lock'

__LOGCFG = {
    'version': 1,

    'formatters': {
        'void': {
            'format': ''
        },
        'standard': {
            'format': '[%(levelname)s] %(message)s'
            # 'format': ':: %(asctime)s [%(levelname)s] %(message)s'
        },
        'cherrypy_formatter': {
            'format': '[%(levelname)s] %(message)s'
            # 'format': ':: %(asctime)s [%(levelname)s] %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout'
        },
        'cherrypy_access': {
            'level':'WARNING',
            'class':'logging.StreamHandler',
            'formatter': 'cherrypy_formatter',
            'stream': 'ext://sys.stdout'
        },
        'cherrypy_console': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'cherrypy_formatter',
            'stream': 'ext://sys.stdout'
        },
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': 'DEBUG'
        },
        'cherrypy.access': {
            'handlers': ['cherrypy_access'],
            'level': 'DEBUG',
            'propagate': False
        },
        'cherrypy.error': {
            'handlers': ['cherrypy_console'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
}


def publish(channel, *args, **kwargs):
    cherrypy.engine.publish(channel, *args, **kwargs)

def subscribe(channel, callback, priority=None):
    cherrypy.engine.subscribe(channel, callback, priority)


class Object:
    pass


class Api():
    exposed = True
    def __init__(self, module):
        self.module = module

    def GET(self):
        return 'not implemented'

    def _cp_dispatch(self, vpath):
        logging.debug(f'vpath: {vpath}')
        # if len(vpath) == 1:
        #     cherrypy.request.params['id'] = vpath.pop()
        #     return self
        if len(vpath) == 2:
            cherrypy.request.params['id'] = vpath.pop(0)
            return self
        return vpath


class Module(threading.Thread):
    CFG_SECTION = 'module'
    CFG_DEFAULTS = {}

    def __init__(self):
        super().__init__()
        self._api = Api(self)
        self.cfg = configparser.ConfigParser(
            defaults=self.CFG_DEFAULTS,
            default_section=self.CFG_SECTION)

    def stop(self):
        pass

    def run(self):
        pass

    def api(self, name_or_apiclass, apiclass=None):
        if apiclass is None:
            apiclass = name_or_apiclass
            self._api = apiclass(self)
        else:
            name = name_or_apiclass
            setattr(self._api, name, apiclass(self))

    def load_config(self):
        try:
            self.cfg.read(CFGFILE)
        except Exception as ex:
            logging.error(ex)

    def save_config(self):
        with open(CFGFILE, 'w') as f:
            self.cfg.write(f)


class Root(): pass


class App:
    CFG_SECTION = 'system'
    CFG_DEFAULTS = {
        'modules': 'inputs, outputs, scanners, printers, notify',
        'server_ipv4': '192.168.1.64',
        'server_port': '12080'}

    def __init__(self):
        self.terminate = False
        self.pidfile = lockfile.pidlockfile.PIDLockFile(PIDFILE)
        self.modules = OrderedDict()

    def __init_config(self):
        self.cfg = configparser.ConfigParser(
            defaults=self.CFG_DEFAULTS,
            default_section=self.CFG_SECTION)
        try:
            self.cfg.read(CFGFILE)
        except Exception as ex:
            logging.exception(ex)

    def __load_modules(self):
        try:
            modules = [name.replace(' ', '') for name in self.cfg['system'].get('modules').split(',')]
        except Exception as ex:
            modules = []
            logging.exception(ex)

        modules.append('system')

        conf = {
            '/': {
                'tools.staticfile.on' : True,
                'tools.staticfile.filename' : '/root/app/mc1/index.html'
            }
        }

        cherrypy.tree.mount(Root(), '/debug', conf)

        mod_conf = {
            '/': {
                'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            }
        }
        
        for module in modules:
            modulename = 'module-'+module
            logging.info('loading module: {}'.format(module))

            try:
                importlib.import_module(modulename)
            except ImportError as ex:
                logging.error(ex)
            else:
                self.modules[module] = sys.modules[modulename].Module(self)
                try:
                    self.modules[module]._api
                except:
                    pass
                else:
                    cherrypy.tree.mount(self.modules[module]._api, f'/api/{module}', mod_conf)


    def __start_modules(self):
        for name in self.modules.keys():
            logging.info('starting module: {}'.format(name))
            self.modules[name].start()

    def __stop_modules(self):
        for name in self.modules.keys():
            logging.info('stopping module: {}'.format(name))
            self.modules[name].stop()

    def start(self):
        logging.info('MC1 started, PID: {}'.format(self.pidfile.read_pid()))

        self.__init_config()
        self.__load_modules()
        self.__start_modules()

        cherrypy.server.socket_host = '0.0.0.0'
        cherrypy.engine.start()
        while not self.terminate:
            time.sleep(1)
        cherrypy.engine.exit()
        self.__stop_modules()

        for thread in self.modules.values():
            try:
                thread.join()
            except RuntimeError:
                pass
        logging.info('MC1 stopped')

    def stop(self, signum, frame):
        logging.info('stopping MC1')
        self.terminate = True


def start():
    tries = 10
    while tries > 0:
        try:
            with daemon.DaemonContext(
                    stdout=sys.stdout,
                    stderr=sys.stderr,
                    chroot_directory=None,
                    working_directory='/',
                    signal_map={
                        signal.SIGTERM: app.stop,
                        signal.SIGTSTP: app.stop},
                    pidfile=app.pidfile):
                app.start()
            return
        except lockfile.AlreadyLocked:
            logging.error('PID lock file exists')
        tries -= 1
        time.sleep(1)


def stop():
    pid = app.pidfile.read_pid()
    if pid == None:
        return
    try:
        os.kill(pid, signal.SIGTERM)
    except OSError as ex:
        logging.error('failed to terminate {pid:d}: {ex}'.format(pid=pid, ex=ex))


def restart():
    stop()
    start()


if __name__ == '__main__':
    # setup logging
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.config.dictConfig(__LOGCFG)

    # patch CherryPy logging
    def _cp_error(self, msg='', context='', severity=logging.INFO, traceback=False):
        if traceback:
            msg += cherrypy._cperror.format_exc()
        self.error_log.log(severity, 'cherrypy: '+' '.join((context, msg)))
    cherrypy.log.error = types.MethodType(_cp_error, cherrypy.log)
    
    app = App()

    parser = argparse.ArgumentParser(description='MC1 system controller service.')
    parser.add_argument('action', choices=['start', 'stop', 'restart'])
    # parser.add_argument('start', help='start daemon', action='store_true')
    # parser.add_argument('stop', help='stop daemon', action='store_true')
    # parser.add_argument('reload', help='reload daemon', action='store_true')
    args = parser.parse_args()

    if args.action=='stop':
        stop()
    elif args.action=='restart':
        restart()
    elif args.action=='start':
        start()
